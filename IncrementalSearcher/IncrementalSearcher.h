#pragma once

#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>
#include <list>
#include <set>
#include <filesystem>
#include <unordered_map>
#include <algorithm>

struct IncrementalSearcher {

	IncrementalSearcher(const std::set<std::string>& db);

	void push(char c);

	void pop();

	void erase();

	std::string search_string() const;

	std::set<std::string_view>::difference_type selected_match_index() const;

	std::string_view selected_match() const;

	// Returns false if selection reached end of last search step
	bool next_selection();

	bool prev_selection();

	std::string_view longest_match() const;

	using SearchStep = std::pair<char, std::set<std::string_view>>;
	std::list<SearchStep> steps;
	const std::set<std::string>& db;
	std::set<std::string_view>::difference_type selection_index;
};
