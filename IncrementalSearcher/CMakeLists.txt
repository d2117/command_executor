cmake_minimum_required(VERSION 3.0)

add_library(incremental_searcher OBJECT ${CMAKE_CURRENT_SOURCE_DIR}/IncrementalSearcher.cpp)
