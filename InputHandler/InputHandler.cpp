#include <InputHandler.h>



InputHandler::InputHandler(XlibHandlers& xhandlers) : xhandlers(xhandlers) { }

InputHandler::~InputHandler() = default;

InputHandler::InputData InputHandler::read_input() {

	auto& [ d, s, gc, font_info, font_height, w ] = xhandlers;

	XEvent e;
	XNextEvent(d, &e);

	if(KeyPress == e.type) {
		return { 
			XLookupKeysym(&e.xkey, 0), 
			static_cast<KEY_MASK>(e.xkey.state),
			get_input_char(e)
		}; 
	}

	return {};
}

char InputHandler::get_input_char(XEvent& e) {
	char input_char;
	KeySym key_sym_buffer;
	XLookupString(&e.xkey, &input_char, sizeof(input_char), &key_sym_buffer, NULL);
	return input_char;
}
