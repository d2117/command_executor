#pragma once

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

struct XlibHandlers {
	Display* d;
	int s;
	GC gc;
	XFontStruct* font_info;
	int font_height;
	Window w;
};
