#include <CommandExecutor.h>

namespace {
	constexpr char start_message[] = ">...";
}

CommandExecutor::CommandExecutor()
	: window(std::make_unique<X11_Window>(xhandlers))
		, provider(std::make_unique<BinListProvider>(std::vector<std::string>{"/usr/bin"}))
		, searcher(std::make_unique<IncrementalSearcher>(provider->binaries.at("/usr/bin")))
		, ihandler(std::make_unique<InputHandler>(xhandlers)) { }

		void CommandExecutor::run() {

			ihandler->read_input(); // trigger to launch ui event queue (xlib specific)
			window->draw_start_message(start_message); 

			while(true) {

				auto [ key_sym, key_mask, input_char ] = ihandler->read_input();

				if(key_sym_to_ignore(key_sym))
					continue;

				if(key_mask == KEY_MASK::NONE) {
					if(!single_key(key_sym, input_char)) {
						break;
					}
					continue;
				}

				if(key_mask == KEY_MASK::CTRL) {
					key_with_ctrl(key_sym);
					continue;
				}

				if(key_mask == KEY_MASK::SHIFT) {
					key_with_shift(key_sym, input_char);
					continue;
				}

			}
		}

bool CommandExecutor::key_sym_to_ignore(int key_sym) {

	switch(key_sym) {
		case XK_Tab:
		case XK_Up:
		case XK_Down:
		case XK_Caps_Lock:
		case XK_space:
		case XK_Alt_L:
		case XK_Alt_R:
		case XK_Shift_L:
		case XK_Shift_R:
		case XK_Control_L:
		case XK_Control_R: return true;
	}

	return false;
}

void CommandExecutor::key_with_ctrl(int key_sym) {

	switch(key_sym) {

		case XK_w: { reset_window(); break; }

		case XK_n: { select_next(); break; }

		case XK_b: { select_prev(); break; }
	}
}

void CommandExecutor::reset_window() {
	erase_input();
	window->draw_start_message(start_message); 
}

void CommandExecutor::select_next() {
	if(searcher->next_selection())
		redraw_window();
}

void CommandExecutor::select_prev() {
	if(searcher->prev_selection())
		redraw_window();
}

void CommandExecutor::key_with_shift(int key_sym, char input) {

	switch(key_sym) {

		case XK_minus: { update_search(input); break; }

		default: { update_search(input); break; }
	}

}

bool CommandExecutor::single_key(int key_sym, char input) {
	switch(key_sym) {

		case XK_Escape: return false;

		case XK_Return: { execute_bin(); return false; }

		case XK_BackSpace: { remove_input_char(); break; }

		case XK_Left: { key_with_ctrl(XK_b); break; }

		case XK_Right: { key_with_ctrl(XK_n); break; }

		default: { update_search(input); break; }
	}
	return true;
}

void CommandExecutor::remove_input_char() {

	searcher->pop();

	if(searcher->steps.empty()) {
		window->clear();
		window->draw_start_message(start_message);
		return;
	}

	redraw_window();
}

bool CommandExecutor::execute_bin() {

	if(searcher->steps.empty()) return false;
	if(searcher->steps.back().second.empty()) return false;

	BinExecutioner{}.execute(std::string("/usr/bin/") + std::string(searcher->selected_match()), {});
	return true;
}

void CommandExecutor::update_search(char input) {
	searcher->push(input);
	redraw_window();
}

void CommandExecutor::erase_input() {
	searcher->erase();
	window->clear();
}

void CommandExecutor::redraw_window() {

	window->clear();

	if(!searcher->steps.empty()) {
		const auto& matches = searcher->steps.back().second;

		window->draw_matches(matches);
		window->draw_selection(searcher->selected_match_index());
	}

	window->draw_user_input(searcher->search_string());
}
