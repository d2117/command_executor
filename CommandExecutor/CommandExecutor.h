#pragma once

#include <InputHandler.h>
#include <BinListProvider.h>
#include <BinExecutor.h>
#include <IncrementalSearcher.h>
#include <X11_Window.h>

#include <memory>

class CommandExecutor {

	public:

		CommandExecutor();

		void run();

	private:

		bool key_sym_to_ignore(int key_sym);

		void key_with_ctrl(int key_sym);

		void key_with_shift(int key_sym, char input);

		bool single_key(int key_sym, char input);

		void erase_input();

		void redraw_window();

		void update_search(char input);

		bool execute_bin();

		void remove_input_char();

		void select_next();

		void select_prev();

		void reset_window();

	private:

		XlibHandlers xhandlers;
		std::unique_ptr<X11_Window> window;
		std::unique_ptr<BinListProvider> provider;
		std::unique_ptr<IncrementalSearcher> searcher;
		std::unique_ptr<InputHandler> ihandler;
};
