#include <BinListProvider.h>

BinListProvider::BinListProvider(std::vector<std::string>&& path_list) {

	for(const auto& dir: path_list) {

		std::set<std::string> files;

		std::filesystem::path bin_dir { dir };
		for(const auto& file: std::filesystem::directory_iterator { bin_dir }) {
			if(!file.is_directory() && is_file_executable(file.path())) {
				files.emplace(file.path().filename());
			}
		}

		if(!files.empty()) {
			binaries.emplace(dir, std::move(files));
		}

	}
}

void BinListProvider::print_binaries() const {
	std::size_t entries_counter {0ul};
	for(const auto& [prefix, files]: binaries) {
		std::cout << prefix << ":\n\n";
		for(const auto& file: files) {
			std::cout << file << "\n";
		}
		entries_counter += files.size();
	}
	std::cout << "Total files count: " << entries_counter << "\n";
}

std::string_view BinListProvider::longest_binary_name() const {
	std::list<std::string_view> longest_from_dirs;
	for(const auto& [ bin_dir, files ]: binaries) {
		const auto longest_in_dir = std::max_element( files.begin(), files.end(), [](const auto& current, const auto& next) { return current.size() < next.size();});
		longest_from_dirs.emplace_back(longest_in_dir->data(), longest_in_dir->size());
	}
	return *std::max_element(longest_from_dirs.begin(), longest_from_dirs.end(), [](const auto& current, const auto& next) { return current.size() < next.size();});
}

bool BinListProvider::is_file_executable(const std::string& path) const {
	const auto file_perms = std::filesystem::status(path).permissions();
	using perms = std::filesystem::perms;
	return (file_perms & perms::owner_exec) != perms::none;
}


